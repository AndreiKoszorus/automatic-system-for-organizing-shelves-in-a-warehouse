#include <Servo.h>
#include "WiFiEsp.h"
#include <SPI.h>
#include <MFRC522.h>
#include <ArduinoJson.h>

#define mLeft0 6
#define mLeft1 4
#define mRight0 3
#define mRight1 2
#define enLeft 9
#define enRight 10

#define leftFrontSensor 22
#define rightFrontSensor 24
#define leftSideSensor 28
#define rightSideSensor 26

#define servoPin 11

#define POSSIBLE_DELIVERY 10
#define ROW 1
#define COLUMN 1
#define ROWS 3
#define COLS 3

#define SS_PIN 53
#define RST_PIN 5

#define SERVER_ADDRESS "172.20.10.2"

String response = "";

enum actionStates {
  START,
  CHECK_COORDINATE_LIST,
  FIND_ROW_START,
  FIND_ROW_DELIVERY,
  FIND_COLUMN,
  TAKE_OBJECT,
  GO_TO_DELIVERY_PLACE,
  GO_TO_START_PLACE,
  GO_BACK_TO_COLUMN_LINE,
  PLACE_OBJECT_DELIVERY,
  SCAN_OBJECT,
  SEND_RF_ID,
  RECEIVE_COORDINATES,
  PLACE_OBJECT,
  TAKE_OBJECT_START,
  TEST,
  GO_TO_START
};

enum robotStates {
  BUSY,
  READY,
  DELIVERY,
  TO_DELIVERY,
  PLACE_OBJ,
  TAKE_OBJ,
  TO_START
};

typedef struct Coordinate {
  int column;
  int row;
};

char ssid[] = "Koszo";
char password[] = "12345678";

int stat = WL_IDLE_STATUS;
int reqCount = 0;
WiFiEspServer server(80);
WiFiEspClient arduinoClient;

RingBuffer buf(8);
MFRC522 mfrc522(SS_PIN, RST_PIN);
int leftSensorState;
int rightSensorState;
int rightSideSensorState;
int leftSideSensorState;
int turning_180_phase = 0;
int turn_90_right_phase = 0;
int turn_90_left_phase = 0;

unsigned long currentTime = 0;
unsigned long currentMillis = 0;
boolean move_180 = false;

boolean end_line = false;

boolean move_right = false;
boolean move_left = false;
boolean state = true;
boolean allowToMove = true;
boolean moveBack = false;
boolean forward = false;
boolean moveLeftIntersection = false;
int robotDirection = 0;

boolean testServo = true;

boolean forkliftUpFinished = false;

boolean forkliftDownFinished = false;

boolean moveForkliftUp = false;
boolean moveForkliftDown = false;

unsigned long servoCurrentTime = 0;

Servo myServo;

enum actionStates actionState;
enum robotStates robotState;


boolean moveLeftFinished = false;
boolean moveRightFinished = false;
boolean move180Finished = false;
boolean moveBackFinished = false;

boolean freezeTime = true;
boolean elapsedSecond = false;

Coordinate CoordinateList[POSSIBLE_DELIVERY];
int freePlaceIndex = 0;
int currentProcessingIndex = 0;

Coordinate currentCoordinate;

int countRow = 0;
int countCol = 0;

boolean canCountCol = true;
boolean canCountRow = true;

boolean canMove180 = true;
boolean rotateRightIntersection = false;
boolean rotateLeftIntersection = false;
boolean canMoveBack = true;
boolean canMoveForward = true;
boolean freezeTime2 = true;
int getObjStartPhase = 0;
boolean arrivedToStart = false;
int realCoordinate = 0;

boolean takeCommand = false;

const char* remote_addr = "www.google.com";
IPAddress ip(172, 20, 10, 13);
int test = 1;
String command = "";
String location = "{";
String content = "";
String baseUrl = "POST /containers/new_container/";
int motorPower = 90;

boolean canCount = true;
boolean stall = true;
boolean freezTimeFwd = true;
boolean rightMode = false;
boolean allowRightMode = true;
void setup() {
  Serial.begin(115200);
  Serial1.begin(115200);

  SPI.begin(); // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522

  WiFi.init(&Serial1);
  //motor setup
  pinMode(mLeft0, OUTPUT);
  pinMode(mLeft1, OUTPUT);
  pinMode(enLeft, OUTPUT);

  pinMode(mRight0, OUTPUT);
  pinMode(mRight1, OUTPUT);
  pinMode(enRight, OUTPUT);

  //ir sensors setup
  pinMode(leftFrontSensor, INPUT);
  pinMode(rightFrontSensor, INPUT);
  pinMode(leftSideSensor, INPUT);
  pinMode(rightSideSensor, INPUT);

  //servo
  myServo.attach(servoPin);
  myServo.write(87);

  actionState = START;

  while ( stat != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    stat = WiFi.begin(ssid, password);
    espDrv.config(ip);

  }
  Serial.println("You're connected to the network");

  // start the web server on port 80
  server.begin();

  //accesare endpoint

}

bool stop_motor() {
  if (freezeTime) {
    currentTime = millis();
    freezeTime = false;
    allowToMove = false;
  }
  if (millis() - currentTime <= 1000) {
    digitalWrite(mLeft0, LOW);
    digitalWrite(mLeft1, LOW);

    digitalWrite(mRight0, LOW);
    digitalWrite(mRight1, LOW);
  }
  else {
    freezeTime = true;
    allowToMove = true;
    return true;
  }

  return false;

}
void turn_180() {
  move180Finished = false;
  if (allowToMove) {
    digitalWrite(mLeft0, HIGH);
    digitalWrite(mLeft1, LOW);
    analogWrite(enLeft, 120);

    digitalWrite(mRight0, LOW);
    digitalWrite(mRight1, HIGH);
    analogWrite(enRight, 120);
  }
  //Serial.println("turning");
  if ((turning_180_phase == 0) && ((leftSensorState == 0) && (rightSensorState) == 0)) {
    turning_180_phase++;
    //Serial.println("phase 1");
  }
  else if ((turning_180_phase == 1) && (leftSideSensorState == 1) && (leftSensorState == 1)) {
    //  Serial.println("finished");
    turning_180_phase = 0;
    move180Finished = true;
    move_180 = false;
    //forward = true;

  }
}

void move_back() {

  moveBackFinished = false;
  if (freezeTime) {
    currentTime = millis();
    freezeTime = false;
  }
  else {
    digitalWrite(mLeft0, HIGH);
    digitalWrite(mLeft1, LOW);
    analogWrite(enLeft, 100);

    digitalWrite(mRight0, HIGH);
    digitalWrite(mRight1, LOW);
    analogWrite(enRight, 100);

    if (millis() - currentTime > 300) {
      moveBackFinished = true;
      moveBack = false;
      freezeTime = true;

    }
  }
}

void turn_90_right() {
  moveRightFinished = false;
  if (allowToMove) {
    digitalWrite(mLeft0, LOW);
    digitalWrite(mLeft1, HIGH);
    analogWrite(enLeft, 120);

    digitalWrite(mRight0, LOW);
    digitalWrite(mRight1, LOW);
  }
  if (!rightMode) {
    if ((turn_90_right_phase == 0) && (rightSideSensorState == 1)) {
      turn_90_right_phase++;
    }
    else if ((rightSideSensorState == 0) && (turn_90_right_phase == 1)) {

      turn_90_right_phase = 0;
      //verificare rightSideSensor == 1
      moveRightFinished = true;
      move_right = false;

    }
  }
  else{
     if ((rightSensorState== 1) && (turn_90_right_phase == 0)) {

      turn_90_right_phase = 0;
      moveRightFinished = true;
      move_right = false;

    }
  }
}

void turn_90_left() {
  moveLeftFinished = false;

  if (allowToMove) {
    digitalWrite(mLeft0, LOW);
    digitalWrite(mLeft1, LOW);

    digitalWrite(mRight0, LOW);
    digitalWrite(mRight1, HIGH);
    analogWrite(enRight, 120);
  }
  if (!moveLeftIntersection) {
    if ((turn_90_left_phase == 0) && (rightSideSensorState == 1)) {
      turn_90_left_phase++;
    }
    else if ((leftSideSensorState == 0) && (turn_90_left_phase == 1)) {
      if (leftSensorState == 1) {
        turn_90_left_phase = 0;
        moveLeftFinished = true;
        move_left = false;
      }
    }
  }
  else {
    Serial.println("other leeeeeeeeeeeeeeeeeeeeeeeft");
    if ((turn_90_left_phase == 0) && (leftSideSensorState == 1)) {
      turn_90_left_phase++;
    }
    else if ((leftSideSensorState == 0) && (turn_90_left_phase == 1)) {
      if (leftSensorState == 1) {
        turn_90_left_phase = 0;
        moveLeftFinished = true;
        move_left = false;
        moveLeftIntersection = false;
      }
    }
  }
}


void follow_line() {

  if (allowToMove) {

    end_line = false;
    if ((leftSensorState == 1) && (rightSensorState == 1)) {
      canCount = true;
      digitalWrite(mLeft0, LOW);
      digitalWrite(mLeft1, HIGH);
      analogWrite(enLeft, 95);

      digitalWrite(mRight0, LOW);
      digitalWrite(mRight1, HIGH);
      analogWrite(enRight, 95);


    }

    else if ((leftSensorState == 0) && (rightSensorState == 1)) {
      canCount = true;
      digitalWrite(mLeft0, LOW);
      digitalWrite(mLeft1, HIGH);
      analogWrite(enLeft, 95);

      digitalWrite(mRight0, LOW);
      digitalWrite(mRight1, LOW);
    }


    else if ((leftSensorState == 1) && (rightSensorState == 0)) {
      canCount = true;
      digitalWrite(mLeft0, LOW);
      digitalWrite(mLeft1, LOW);

      digitalWrite(mRight0, LOW);
      digitalWrite(mRight1, HIGH);
      analogWrite(enRight, 95);


    }
    else if ((rightSensorState == 0) && (leftSensorState == 0) && (rightSideSensorState == 1)) {
      canCount = false;
      digitalWrite(mLeft0, LOW);
      digitalWrite(mLeft1, HIGH);
      analogWrite(enLeft, 80);

      digitalWrite(mRight0, LOW);
      digitalWrite(mRight1, LOW);

    }
    else if ((rightSensorState == 0) && (leftSensorState == 0) && (leftSideSensorState == 1)) {
      canCount = false;
      digitalWrite(mLeft0, LOW);
      digitalWrite(mLeft1, LOW);

      digitalWrite(mRight0, LOW);
      digitalWrite(mRight1, HIGH);
      analogWrite(enRight, 80);
    }
    else if ((rightSensorState == 0) && (leftSensorState == 0) && (rightSideSensorState == 0) && (leftSideSensorState == 0)) {
      canCount = true;
      end_line = true;
      currentTime = millis();
      servoCurrentTime = millis();
    }
  }

}

void move_forklift_up() {
  forkliftUpFinished = false;
  if (millis() - servoCurrentTime < 7000) {
    myServo.write(180);
  }
  else {
    myServo.write(87);
    forkliftUpFinished = true;
    moveForkliftUp  = false;
  }

}

void move_forklift_down() {
  forkliftDownFinished = false;
  if (millis() - servoCurrentTime < 7000) {
    myServo.write(0);
  }
  else {
    myServo.write(87);
    forkliftDownFinished = true;
    moveForkliftDown  = false;
  }

}

void stop_movement() {
  digitalWrite(mLeft0, LOW);
  digitalWrite(mLeft1, LOW);

  digitalWrite(mRight0, LOW);
  digitalWrite(mRight1, LOW);
}

void loop() {

  // put your main code here, to run repeatedly:
  leftSensorState = digitalRead(leftFrontSensor);
  rightSensorState = digitalRead(rightFrontSensor);
  rightSideSensorState = digitalRead(rightSideSensor);
  leftSideSensorState = digitalRead(leftSideSensor);

  WiFiEspClient client = server.available();
  if (client) {
    Serial.println("New client");
    command = "";
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    delay(20);
    while (client.connected()) {
      if (client.available()) {

        char c = client.read();
        command += c;
        if (c == '\n' && currentLineIsBlank) {
          Serial.println("Sending response");

          // send a standard http response header
          // use \r\n instead of many println statements to speedup data send
          client.print(
            "HTTP/1.1 200 OK\r\n"
            "Content-Type: text/html\r\n"
            "Connection: close\r\n"  // the connection will be closed after completion of the response
            "\r\n");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    delay(20);
    takeCommand = true;
    Coordinate newCoordinate;
    StaticJsonDocument<200> document;

    DeserializationError error = deserializeJson(document, command);
    if (error) {
      Serial.print(F("deserializeJson() take command failed: "));
      Serial.println(error.c_str());
      return;
    }

    newCoordinate.row = document["row"].as<int>();
    newCoordinate.column = document["column"].as<int>();
    CoordinateList[freePlaceIndex] = newCoordinate;
    freePlaceIndex++;
    // give the web browser time to receive the data
    delay(10);

    Serial.println(document["row"].as<int>());
    Serial.println(document["column"].as<int>());

    // close the connection:
    client.stop();
    Serial.println("Client disconnected");

  }

  //Serial.println("ENTER");
  if (forward) {

    follow_line();

  }
  if (move_180) {
    turn_180();
  }

  if (move_right) {
    turn_90_right();
  }

  if (move_left) {
    turn_90_left();
  }

  if (moveForkliftUp) {
    move_forklift_up();
  }

  if (moveForkliftDown) {
    move_forklift_down();
  }

  if (moveBack) {
    move_back();
  }


  switch (actionState) {
    case START:
      allowToMove = false;
      stop_movement();
      //Serial.println("START");
      robotState = READY;
      end_line = false;
      if (takeCommand) {
        robotState = TAKE_OBJ;
        actionState = CHECK_COORDINATE_LIST;
        allowToMove = true;
        forward = true;
        stall = true;
      }
      else {
        robotState = PLACE_OBJ;
        baseUrl = "POST /containers/new_container/";
        content = "";
        location = "{";
        //Serial.println("SCAAAN");
        if ( !mfrc522.PICC_IsNewCardPresent())
        {
          return;
        }
        // Select one of the cards
        if ( ! mfrc522.PICC_ReadCardSerial())
        {
          return;
        }
        //Show UID on serial monitor
        Serial.print("UID tag :");

        byte letter;
        for (byte i = 0; i < mfrc522.uid.size; i++)
        {
          Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? "0" : "");
          Serial.print(mfrc522.uid.uidByte[i], HEX);
          content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
          content.concat(String(mfrc522.uid.uidByte[i], HEX));
        }
        content.toUpperCase();

        if (arduinoClient.connect(SERVER_ADDRESS, 8080)) {
          delay(50);
          Serial.println("Connected to server");
          baseUrl += content;
          arduinoClient.println(baseUrl + " HTTP/1.1");
          arduinoClient.println("Host: 172.20.10.2");
          arduinoClient.println("Connection: close");
          arduinoClient.println();
        }

        char headers[300] = {0};
        arduinoClient.readBytesUntil('{', headers, sizeof(headers));
        delay(50);
        while (arduinoClient.available()) {
          location += (char) arduinoClient.read();
        }
        Serial.println(content);
        StaticJsonDocument<200> doc;

        DeserializationError error = deserializeJson(doc, location);
        if (error) {
          Serial.print(F("deserializeJson() failed: "));
          Serial.println(error.c_str());
          return;
        }


        // Extract values
        Serial.println(F("Response:"));
        currentCoordinate.row = doc["row"].as<int>();
        currentCoordinate.column = doc["column"].as<int>();
        actionState = TAKE_OBJECT_START;
        forward = true;
        stall = true;
        allowToMove = true;

        arduinoClient.stop();

      }
      break;

    case CHECK_COORDINATE_LIST:
      if (currentProcessingIndex == freePlaceIndex) {
        currentProcessingIndex = 0;
        freePlaceIndex = 0;
        actionState = GO_TO_START;
        robotState = TO_START;
        takeCommand = false;
      }

      else {
        currentCoordinate = CoordinateList[currentProcessingIndex];
        currentProcessingIndex++;
        if (robotState == PLACE_OBJ || robotState == TAKE_OBJ) {
          actionState = FIND_ROW_START;
        }
        else if (robotState == DELIVERY) {
          actionState = FIND_ROW_DELIVERY;
          robotState = TAKE_OBJ;
        }
      }
      break;

    case FIND_ROW_START:

      //countRow -1 == currentCoordinate.row
      if (robotState == TAKE_OBJ) {
        realCoordinate = currentCoordinate.row;
      }
      else if (robotState == PLACE_OBJ) {
        realCoordinate = currentCoordinate.row + 1;
      }
      if (leftSideSensorState == 0) {
        canCountRow = true;
      }
      if (countRow == currentCoordinate.row) {
        if (forward) {
          if (stop_motor()) {
            move_left = true;
            forward = false;
          }
        }
        else if (moveLeftFinished) {
          if (stop_motor()) {
            countRow = 0;
            actionState = FIND_COLUMN;
            //motorPower = 100;
            stall = true;
            forward = true;
          }
        }
      }
      else if ((leftSideSensorState == 1) && (rightSideSensorState == 0) && (leftSensorState == 1 || rightSensorState == 1)) {
        if (canCountRow && canCount)
          countRow++;
        canCountRow = false;
      }
      break;

    case FIND_ROW_DELIVERY:
      if (rightSideSensorState == 0) {
        canCountRow = true;
      }
      if ((ROWS + 1) - countRow == currentCoordinate.row) {
        if (forward) {
          if (stop_motor()) {
            move_right = true;
            forward = false;
          }
        }
        else if (moveRightFinished) {
          if (stop_motor()) {
            countRow = 0;
            actionState = FIND_COLUMN;
            stall = true;
            forward = true;
          }
        }
      }
      else if ((rightSideSensorState == 1) && (leftSideSensorState == 0) && (leftSensorState == 1 || rightSensorState == 1)) {
        if (canCountRow && canCount)
          countRow++;
        canCountRow = false;
      }
      break;

    case FIND_COLUMN:
      Serial.println("find column");
      if (leftSideSensorState == 0) {
        canCountCol = true;
      }
      if (countCol == currentCoordinate.column) {
        if (forward) {
          if (stop_motor()) {
            move_left = true;
            forward = false;
          }

        }
        else if (moveLeftFinished) {
          if (stop_motor()) {
            if (robotState == TAKE_OBJ) {
              actionState = TAKE_OBJECT;
            }
            else if (robotState == PLACE_OBJ) {
              actionState = PLACE_OBJECT;
            }
            forward = true;
            stall = true;
            countCol = 0;
          }
        }

      }
      else if ((leftSideSensorState == 1) && (rightSideSensorState == 0) && (leftSensorState == 1 || rightSensorState == 1)) {
        if (canCountCol && canCount)
          countCol++;
        canCountCol = false;
      }
      break;

    case TAKE_OBJECT:
      Serial.println("TAKE_OBJ");
      if (end_line) {
        if (forward) {
          if (stop_motor()) {
            moveForkliftUp = true;
            forward = false;
          }
        }
        else if (forkliftUpFinished) {
          if (canMove180) {
            move_180 = true;
            canMove180 = false;
          }
          else if (move180Finished) {
            if (stop_motor()) {
              actionState = GO_BACK_TO_COLUMN_LINE;
              robotState = TO_DELIVERY;
              forward = true;
              stall = true;
              canMove180 = true;
              end_line = false;
              //motorPower = 100;
            }
          }
        }

      }
      break;

    case GO_BACK_TO_COLUMN_LINE:
      Serial.println("go to column line");
      if (forward) {
        if (leftSideSensorState == 1 && rightSideSensorState == 1) {
          rotateRightIntersection = true;
        }
        if (rotateRightIntersection) {
          if (stop_motor()) {
            if(rightSensorState == 0 && leftSensorState ==0 ){
              rightMode = true;
            }
            else{
              rightMode = false;
            }
            move_right = true;
            forward = false;
            rotateRightIntersection = false;
          }
        }
      }
      else if (moveRightFinished) {
        if (stop_motor()) {
          if (robotState == TO_DELIVERY)
            actionState = GO_TO_DELIVERY_PLACE;
          else if (robotState == TO_START)
            actionState = GO_TO_START_PLACE;
          forward = true;
          stall = true;
        }
      }

      break;
    case GO_TO_DELIVERY_PLACE:
      Serial.println("go to delivery place");
      if (forward) {
        if (leftSideSensorState == 1 && rightSideSensorState == 1) {
          rotateLeftIntersection = true;
        }
        if (rotateLeftIntersection) {
          if (stop_motor()) {
            move_left = true;
            moveLeftIntersection = true;
            forward = false;
            rotateLeftIntersection = false;
          }
        }
      }
      else if (moveLeftFinished) {
        if (stop_motor()) {
          actionState = PLACE_OBJECT_DELIVERY;
          forward = true;
          stall = true;
        }
      }
      break;
    case GO_TO_START_PLACE:
      Serial.println("GO to start place");
      if (forward) {
        if (leftSideSensorState == 1 && rightSideSensorState == 1) {
          rotateRightIntersection = true;
        }
        if (rotateRightIntersection) {
          if (stop_motor()) {
            if(rightSensorState == 0 && leftSensorState ==0 ){
              rightMode = true;
            }
            else{
              rightMode = false;
            }
            move_right = true;
            forward = false;
            rotateRightIntersection = false;
          }
        }
      }
      else if (moveRightFinished) {
        if (stop_motor()) {
          actionState = GO_TO_START;
          forward = true;
          stall = true;
        }
      }
      break;

    case PLACE_OBJECT_DELIVERY:
      if (end_line) {
        if (forward) {
          if (stop_motor()) {
            moveForkliftDown = true;
            forward = false;
          }
        }
        else if (forkliftDownFinished) {
          if (canMoveBack) {
            moveBack = true;
            canMoveBack = false;
          }
          else if (moveBackFinished) {
            if (canMove180) {
              if (stop_motor()) {
                move_180 = true;
                canMove180 = false;
              }
            }
            else if (move180Finished) {
              if (stop_motor()) {
                actionState = CHECK_COORDINATE_LIST;
                forward = true;
                stall = true;
                robotState = DELIVERY;
                canMove180 = true;
                canMoveBack = true;
                end_line = false;
                // motorPower = 90;
              }
            }
          }
        }
      }
      break;

    case PLACE_OBJECT:
      Serial.println("place object");
      if (end_line) {
        if (forward) {
          if (stop_motor()) {
            moveForkliftDown = true;
            forward = false;
          }
        }
        else if (forkliftDownFinished) {
          if (canMoveBack) {
            moveBack = true;
            canMoveBack = false;
          }
          else if (moveBackFinished) {
            if (canMove180) {
              if (stop_motor()) {
                move_180 = true;
                canMove180 = false;
              }
            }
            else if (move180Finished) {
              if (stop_motor()) {
                robotState = TO_START;
                actionState = GO_BACK_TO_COLUMN_LINE;
                forward = true;
                stall = true;
                canMove180 = true;
                canMoveBack = true;
                end_line = false;
                // motorPower = 90;
              }
            }
          }
        }
      }
      break;

    case GO_TO_START:
      Serial.println("go to start");
      //trebuie sa se opreasca la linie neagra
      if (leftSideSensorState == 1 && rightSideSensorState == 1) {
        arrivedToStart = true;
      }
      if (arrivedToStart) {
        if (forward) {
          if (stop_motor()) {
            move_180 = true;
            forward = false;
          }
        }
        else if (move180Finished) {
          end_line = false;
          actionState = START;
          arrivedToStart = false;
        }
      }
      break;

    case RECEIVE_COORDINATES:
      actionState = TAKE_OBJECT_START;
      break;

    case TAKE_OBJECT_START:
      //Serial.println("TAKE obj start");
      if (freezeTime2) {
        currentTime = millis();
        freezeTime2 = false;
      }
      else {
        if ((millis() - currentTime > 700) && (getObjStartPhase == 0))  {
          getObjStartPhase++;
        }
        if (forward && getObjStartPhase == 1) {
          if (stop_motor()) {
            forward = false;
            move_180 = true;
            getObjStartPhase++;
          }
        }
        else if (move180Finished && getObjStartPhase == 2) {
          if (stop_motor()) {
            forward = true;
            stall = true;
            getObjStartPhase++;
          }
        }
        else if (getObjStartPhase == 3) {
          if (end_line) {
            if (stop_motor()) {
              forward = false;
              moveForkliftUp = true;
              getObjStartPhase++;
            }
          }
        }
        else if (getObjStartPhase == 4 && forkliftUpFinished) {
          move_180 = true;
          getObjStartPhase++;
        }
        else if (getObjStartPhase == 5 && move180Finished) {
          if (stop_motor()) {
            freezeTime2 = true;
            forward = true;
            stall = true;
            end_line = false;
            getObjStartPhase = 0;
            actionState = FIND_ROW_START;
            robotState = PLACE_OBJ;
            // motorPower = 100;
          }
        }

      }
      break;
  }

}



