package com.robot.robot.service;

import com.robot.robot.models.AdditionalInfo;
import com.robot.robot.models.Container;
import com.robot.robot.repository.InformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AdditionalInformationService {

    @Autowired
    private InformationRepository informationRepository;


    public void saveInformation(AdditionalInfo additionalInfo){
        informationRepository.save(additionalInfo);
    }

    public AdditionalInfo getContainerInfo(String rfid){
        return informationRepository.findByContainer_Rfid(rfid);
    }

    public void deleteInfoByContainer(Container container){
        informationRepository.deleteAdditionalInfoByContainer(container);
    }

}
