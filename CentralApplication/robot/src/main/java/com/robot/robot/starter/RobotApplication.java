package com.robot.robot.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@EnableJpaRepositories(basePackages = {"com.robot.robot.repository"})
@ComponentScan(basePackages = {"com.robot.robot.controller","com.robot.robot.service","com.robot.robot.repository","com.robot.robot.configuration"})
@EntityScan(basePackages = {"com.robot.robot.models"})
public class RobotApplication {

	public static void main(String[] args) {

		SpringApplication.run(RobotApplication.class, args);
	}

}
