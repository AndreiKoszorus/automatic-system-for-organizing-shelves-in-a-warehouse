package com.robot.robot.controller;

import com.robot.robot.DTO.AdditionalInfoDTO;
import com.robot.robot.models.AdditionalInfo;
import com.robot.robot.models.Container;
import com.robot.robot.service.AdditionalInformationService;
import com.robot.robot.service.ContainerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Column;

@RestController
@RequestMapping(value = "/information")
@CrossOrigin
public class InformationController {
    @Autowired
    private AdditionalInformationService additionalInformationService;

    @Autowired
    private ContainerService containerService;

    @Autowired
    private ModelMapper modelMapper;


    @GetMapping(value = "/get_information/{rfid}", produces = "application/json")
    public AdditionalInfoDTO getContainerInfo(@PathVariable("rfid") String rfid){
        AdditionalInfo additionalInfo = additionalInformationService.getContainerInfo(rfid);
        if(additionalInfo != null)
            return modelMapper.map(additionalInfo, AdditionalInfoDTO.class);

        return null;
    }


    @PostMapping(value = "/add_info/{rfid}", produces = "application/json")
    public void addInfo(@PathVariable("rfid") String rfid, @RequestBody AdditionalInfoDTO additionalInfoDTO){
        Container container = containerService.getContainer(rfid);

        AdditionalInfo additionalInfo = modelMapper.map(additionalInfoDTO,AdditionalInfo.class);
        if(container != null) {
            additionalInfo.setContainer(container);

            additionalInformationService.saveInformation(additionalInfo);
        }
    }

    @PutMapping(value = "/update_info/{rfid}", produces = "application/json")
    public void updateInfo(@PathVariable("rfid") String rfid, @RequestBody AdditionalInfoDTO additionalInfoDTO){
        AdditionalInfo additionalInfo = additionalInformationService.getContainerInfo(rfid);
        System.out.println(additionalInfoDTO.getDepositDate());
        additionalInfo.setDepositDate(additionalInfoDTO.getDepositDate());
        additionalInfo.setItems(additionalInfoDTO.getItems());
        additionalInfo.setTotalWeight(additionalInfoDTO.getTotalWeight());

        additionalInformationService.saveInformation(additionalInfo);
    }

}
