package com.robot.robot.models;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "additional_info")
public class AdditionalInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private int totalWeight;

    @Column
    private Timestamp depositDate;

    @Column
    private String items;

    @OneToOne( targetEntity = Container.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "container_id")
    private Container container;


    public AdditionalInfo(){

    }

    public AdditionalInfo(int totalWeight, Timestamp depositDate, String items,Container container) {
        this.totalWeight = totalWeight;
        this.depositDate = depositDate;
        this.items = items;
        this.container = container;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Timestamp getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Timestamp depositDate) {
        this.depositDate = depositDate;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Container getContainer() {
        return container;
    }

    public void setContainer(Container container) {
        this.container = container;
    }
}
