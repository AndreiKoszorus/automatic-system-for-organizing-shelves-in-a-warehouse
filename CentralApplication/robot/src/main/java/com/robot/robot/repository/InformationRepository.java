package com.robot.robot.repository;

import com.robot.robot.models.AdditionalInfo;
import com.robot.robot.models.Container;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformationRepository extends JpaRepository<AdditionalInfo,Integer> {

    AdditionalInfo findByContainer_Rfid(String rfid);
    void deleteAdditionalInfoByContainer(Container container);
}
