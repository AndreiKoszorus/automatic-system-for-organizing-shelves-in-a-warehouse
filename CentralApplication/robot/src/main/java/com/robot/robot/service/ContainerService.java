package com.robot.robot.service;


import com.robot.robot.models.Container;
import com.robot.robot.repository.ContainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContainerService {

    @Autowired
    ContainerRepository containerRepository;

    public void saveNewContainer(Container container){
        containerRepository.save(container);
    }

    public Container getContainer(String rfid){
        return containerRepository.getByRfid(rfid);
    }

    public void deleteContainer(Container container){
        containerRepository.delete(container);
    }


}
