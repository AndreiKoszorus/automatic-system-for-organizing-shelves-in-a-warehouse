package com.robot.robot.service;

import com.robot.robot.models.Container;
import com.robot.robot.models.Location;
import com.robot.robot.repository.ContainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class LocationService {

    @Autowired
    private ContainerRepository containerRepository;

    private final int gridDimension = 2;



    public Location freeLocation(){
        int freeLocationColumn = 1;
        int freeLocationRow = 1;
        int iteration  = 0;
        int gridElelements = gridDimension * gridDimension;
        ArrayList<Location> locations = new ArrayList<>();
        List<Container> containers = containerRepository.findAll();
        if(containers != null){
            for(Container container:containers){
                locations.add(new Location(container.getRow(),container.getColumn()));
            }
            while(iteration < gridElelements){
                if(freeLocationColumn == gridDimension + 1){
                    freeLocationRow++;
                    freeLocationColumn = 1;
                }

                Location freeLocation = new Location(freeLocationRow,freeLocationColumn);
                if(locations.contains(freeLocation)){
                    freeLocationColumn++;
                }
                else{
                    return freeLocation;
                }
                iteration++;

            }
        }

        return null;
    }
}
