package com.robot.robot.models;

import java.io.Serializable;

public class Location implements Serializable {

    private int row;

    private int column;

    public Location(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public Location(){

    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return "{" +
                "\"row\":" + row +
                ",\"column\":" + column +
                "}";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this){
            return true;
        }

        else if(obj == null){
            return false;
        }

        else if(!(obj instanceof Location)){
            return false;
        }

        Location loc = (Location) obj;

        return this.row == loc.row && this.column == loc.column;
    }
}
