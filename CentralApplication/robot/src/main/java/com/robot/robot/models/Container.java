package com.robot.robot.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "locations")
public class Container implements Serializable,Comparable<Container> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private int column;

    @Column
    private int row;

    @Column
    private String rfid;

    public Container(int column, int row, String rfid) {
        this.column = column;
        this.row = row;
        this.rfid = rfid;
    }

    public Container(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    @Override
    public int compareTo(Container o) {
       if(this.row == o.row){
           if(this.column == o.column){
               return 0;
           }
           else{
               return this.column < o.column ? -1:1;
           }
       }

       return this.row < o.row ? -1:1;
    }

    @Override
    public String toString() {
        return "column: "+this.column + "row: "+this.row;
    }
}
