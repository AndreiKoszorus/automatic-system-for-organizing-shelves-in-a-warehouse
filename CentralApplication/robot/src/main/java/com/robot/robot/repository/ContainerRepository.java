package com.robot.robot.repository;

import com.robot.robot.models.Container;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContainerRepository extends JpaRepository<Container,Integer> {

    Container getByRfid(String rfid);
    List<Container> findAll();

}
