package com.robot.robot.DTO;

import java.io.Serializable;
import java.sql.Timestamp;

public class AdditionalInfoDTO implements Serializable {

    private String items;
    private Timestamp depositDate;
    private int totalWeight;

    public AdditionalInfoDTO(){

    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Timestamp getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Timestamp depositDate) {
        this.depositDate = depositDate;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }
}
