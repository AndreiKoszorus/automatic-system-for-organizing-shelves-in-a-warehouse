package com.robot.robot.controller;

import com.robot.robot.models.Container;

import com.robot.robot.models.Location;
import com.robot.robot.service.ContainerService;
import com.robot.robot.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/containers")
@CrossOrigin
public class ContainerController {
    @Autowired
    ContainerService containerService;

    @Autowired
    LocationService locationService;

    public static final List<SseEmitter> emitters = Collections.synchronizedList( new ArrayList<>());

    @PostMapping(value = "/new_container/{rfid}",produces = "application/json")
    public Location saveNewSpace(@PathVariable("rfid") String rfid){
        System.out.println("New location");
        Location newLocation = locationService.freeLocation();
        Container container = new Container(newLocation.getColumn(),newLocation.getRow(),rfid);
        containerService.saveNewContainer(container);
        sendSseEventsToUI(container);
        return newLocation;
    }

    @GetMapping(value = "/get_location")
    public Location getLocation(){
        return new Location(3,4);
    }

    @RequestMapping(path = "/stream", method = RequestMethod.GET)
    public SseEmitter stream() throws IOException {

        SseEmitter emitter = new SseEmitter();

        emitters.add(emitter);
        emitter.onCompletion(() -> emitters.remove(emitter));

        return emitter;
    }

    public void sendSseEventsToUI(Container notification) { //your model class
        List<SseEmitter> sseEmitterListToRemove = new ArrayList<>();
        ContainerController.emitters.forEach((SseEmitter emitter) -> {
            try {
                emitter.send(notification, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                emitter.complete();
                sseEmitterListToRemove.add(emitter);
                e.printStackTrace();
            }
        });
        ContainerController.emitters.removeAll(sseEmitterListToRemove);
    }


}
