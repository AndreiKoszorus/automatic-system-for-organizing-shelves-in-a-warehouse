package com.robot.robot.controller;


import com.robot.robot.models.AdditionalInfo;
import com.robot.robot.models.Container;
import com.robot.robot.models.Location;
import com.robot.robot.service.AdditionalInformationService;
import com.robot.robot.service.ContainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

@RestController
@RequestMapping(value = "/commands")
@CrossOrigin
public class CommandController {


    @Autowired
    private ContainerService containerService;

    @Autowired
    private AdditionalInformationService additionalInformationService;


    @PostMapping(value = "/send_command",produces = "application/json")
    public void sendCommand(@RequestBody Container container){
        try {
            Location location = new Location(container.getRow(),container.getColumn());
            Socket socket = new Socket("172.20.10.13", 80);
            OutputStream out = socket.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(location.toString());
            bufferedWriter.flush();
            socket.close();

            Container realContainer = containerService.getContainer(container.getRfid());
        AdditionalInfo additionalInfo = additionalInformationService.getContainerInfo(container.getRfid());
        if(additionalInfo != null){
            additionalInformationService.deleteInfoByContainer(realContainer);
        }
        else {
            containerService.deleteContainer(realContainer);
        }

        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
